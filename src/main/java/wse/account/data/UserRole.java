package wse.account.data;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import wse.account.oauth2.user.data.User;


/**
 * The persistent class for the user_role database table.
 *
 */
@Entity
@Table(name="user_role")
@NamedQuery(name="UserRole.findAll", query="SELECT u FROM UserRole u")
public class UserRole implements Serializable {
	private static final long serialVersionUID = 1L;

//	@EmbeddedId
//	private UserRolePK id;

	//bi-directional many-to-one association to Role
	@Id
	@ManyToOne
	@JoinColumn(name="role_id")
	private Role role;

	//bi-directional many-to-one association to User
    @Id
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;

	//bi-directional many-to-one association to Dept
    @Id
	@ManyToOne
	@JoinColumn(name="dept_id")
	private Dept dept;

	public UserRole() {
	}

//	public UserRolePK getId() {
//		return this.id;
//	}
//
//	public void setId(UserRolePK id) {
//		this.id = id;
//	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Dept getDept() {
		return this.dept;
	}

	public void setDept(Dept dept) {
		this.dept = dept;
	}

}