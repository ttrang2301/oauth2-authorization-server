package wse.account.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the service_permission database table.
 * 
 */
@Entity
@Table(name="service_permission")
@NamedQuery(name="ServicePermission.findAll", query="SELECT s FROM ServicePermission s")
public class ServicePermission implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="service_permission_id")
	private int servicePermissionId;

	//bi-directional many-to-many association to Role
	@ManyToMany(mappedBy="servicePermissions")
	private List<Role> roles;

	//bi-directional many-to-one association to Permission
	@ManyToOne
	@JoinColumn(name="permission_id")
	private Permission permission;

	//bi-directional many-to-one association to Service
	@ManyToOne
	@JoinColumn(name="service_id")
	private Service service;

	public ServicePermission() {
	}

	public int getServicePermissionId() {
		return this.servicePermissionId;
	}

	public void setServicePermissionId(int servicePermissionId) {
		this.servicePermissionId = servicePermissionId;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Permission getPermission() {
		return this.permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	public Service getService() {
		return this.service;
	}

	public void setService(Service service) {
		this.service = service;
	}

}