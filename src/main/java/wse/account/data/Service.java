package wse.account.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the service database table.
 * 
 */
@Entity
@NamedQuery(name="Service.findAll", query="SELECT s FROM Service s")
public class Service implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="service_id")
	private int serviceId;

	private byte active;

	@Column(name="client_id")
	private String clientId;

	@Column(name="client_secret")
	private String clientSecret;

	private String module;

	private String name;

	//bi-directional many-to-one association to ServicePermission
	@OneToMany(mappedBy="service")
	private List<ServicePermission> servicePermissions;

	public Service() {
	}

	public int getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return this.clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getModule() {
		return this.module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ServicePermission> getServicePermissions() {
		return this.servicePermissions;
	}

	public void setServicePermissions(List<ServicePermission> servicePermissions) {
		this.servicePermissions = servicePermissions;
	}

	public ServicePermission addServicePermission(ServicePermission servicePermission) {
		getServicePermissions().add(servicePermission);
		servicePermission.setService(this);

		return servicePermission;
	}

	public ServicePermission removeServicePermission(ServicePermission servicePermission) {
		getServicePermissions().remove(servicePermission);
		servicePermission.setService(null);

		return servicePermission;
	}

}