package wse.account.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the role database table.
 * 
 */
@Entity
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="role_id")
	private int roleId;

	private byte active;

	private String name;

	//bi-directional many-to-one association to Type
	@ManyToOne
	@JoinColumn(name="type_id")
	private Type type;

	//bi-directional many-to-many association to ServicePermission
	@ManyToMany
	@JoinTable(
		name="role_service_permission"
		, joinColumns={
			@JoinColumn(name="role_role_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="service_permission_service_permission_id")
			}
		)
	private List<ServicePermission> servicePermissions;

	//bi-directional many-to-one association to UserRole
	@OneToMany(mappedBy="role")
	private List<UserRole> userRoles;

	public Role() {
	}

	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return this.type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public List<ServicePermission> getServicePermissions() {
		return this.servicePermissions;
	}

	public void setServicePermissions(List<ServicePermission> servicePermissions) {
		this.servicePermissions = servicePermissions;
	}

	public List<UserRole> getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public UserRole addUserRole(UserRole userRole) {
		getUserRoles().add(userRole);
		userRole.setRole(this);

		return userRole;
	}

	public UserRole removeUserRole(UserRole userRole) {
		getUserRoles().remove(userRole);
		userRole.setRole(null);

		return userRole;
	}

}