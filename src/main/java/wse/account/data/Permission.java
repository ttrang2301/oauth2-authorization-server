package wse.account.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the permission database table.
 * 
 */
@Entity
@NamedQuery(name="Permission.findAll", query="SELECT p FROM Permission p")
public class Permission implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="permission_id")
	private int permissionId;

	private byte active;

	private String name;

	//bi-directional many-to-one association to ServicePermission
	@OneToMany(mappedBy="permission")
	private List<ServicePermission> servicePermissions;

	public Permission() {
	}

	public int getPermissionId() {
		return this.permissionId;
	}

	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ServicePermission> getServicePermissions() {
		return this.servicePermissions;
	}

	public void setServicePermissions(List<ServicePermission> servicePermissions) {
		this.servicePermissions = servicePermissions;
	}

	public ServicePermission addServicePermission(ServicePermission servicePermission) {
		getServicePermissions().add(servicePermission);
		servicePermission.setPermission(this);

		return servicePermission;
	}

	public ServicePermission removeServicePermission(ServicePermission servicePermission) {
		getServicePermissions().remove(servicePermission);
		servicePermission.setPermission(null);

		return servicePermission;
	}

}