package wse.account.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the oauth_access_token database table.
 *
 */
@Entity
@Table(name="oauth_access_token")
@NamedQuery(name="OauthAccessToken.findAll", query="SELECT o FROM OauthAccessToken o")
public class OauthAccessToken implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	private byte[] authentication;

    @Id
	@Column(name="authentication_id")
	private String authenticationId;

	@Column(name="client_id")
	private String clientId;

	@Column(name="refresh_token")
	private String refreshToken;

	@Lob
	private byte[] token;

	@Column(name="token_id")
	private String tokenId;

	@Column(name="user_name")
	private String userName;

	public OauthAccessToken() {
	}

	public byte[] getAuthentication() {
		return this.authentication;
	}

	public void setAuthentication(byte[] authentication) {
		this.authentication = authentication;
	}

	public String getAuthenticationId() {
		return this.authenticationId;
	}

	public void setAuthenticationId(String authenticationId) {
		this.authenticationId = authenticationId;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getRefreshToken() {
		return this.refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public byte[] getToken() {
		return this.token;
	}

	public void setToken(byte[] token) {
		this.token = token;
	}

	public String getTokenId() {
		return this.tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}