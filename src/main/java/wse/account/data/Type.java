package wse.account.data;

import java.io.Serializable;
import javax.persistence.*;

import wse.account.oauth2.user.data.User;

import java.util.List;


/**
 * The persistent class for the type database table.
 * 
 */
@Entity
@NamedQuery(name="Type.findAll", query="SELECT t FROM Type t")
public class Type implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="type_id")
	private int typeId;

	private String name;

	//bi-directional many-to-one association to Role
	@OneToMany(mappedBy="type")
	private List<Role> roles;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="type")
	private List<User> users;

	public Type() {
	}

	public int getTypeId() {
		return this.typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Role addRole(Role role) {
		getRoles().add(role);
		role.setType(this);

		return role;
	}

	public Role removeRole(Role role) {
		getRoles().remove(role);
		role.setType(null);

		return role;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setType(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setType(null);

		return user;
	}

}