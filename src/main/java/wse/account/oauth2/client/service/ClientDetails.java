package wse.account.oauth2.client.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;

import wse.account.oauth2.client.data.OauthClientDetail;

public class ClientDetails
        implements
            org.springframework.security.oauth2.provider.ClientDetails {

    private static final long serialVersionUID = -7939762350948008744L;

    private String clientId;
    private Set<String> resourceIds;
    private String clientSecret;
    private String scope;
    private Set<String> authorizedGrantTypes;
    private String webServerRedirectUri;
    private Set<String> authorities;

    public ClientDetails(String clientId, Set<String> resourceIds,
            String clientSecret, String scope, Set<String> authorizedGrantTypes,
            String webServerRedirectUri, Set<String> authorities) {
        super();
        this.clientId = clientId;
        this.resourceIds = resourceIds;
        this.clientSecret = clientSecret;
        this.scope = scope;
        this.authorizedGrantTypes = authorizedGrantTypes;
        this.webServerRedirectUri = webServerRedirectUri;
        this.authorities = authorities;
    }

    @Override
    public String getClientId() {
        return this.clientId;
    }

    @Override
    public Set<String> getResourceIds() {
        if (this.resourceIds == null) {
            return new HashSet<String>(0);
        }
        return this.resourceIds;
    }

    @Override
    public boolean isSecretRequired() {
        return (this.clientSecret != null && !this.clientSecret.isEmpty());
    }

    @Override
    public String getClientSecret() {
        return this.clientSecret;
    }

    @Override
    public boolean isScoped() {
        return (this.scope != null || !this.scope.isEmpty());
    }

    @Override
    public Set<String> getScope() {
        if (this.scope == null || this.scope.isEmpty()) {
            return new HashSet<String>(0);
        }
        return new HashSet<String>(Arrays.asList(this.scope));
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        if (this.authorizedGrantTypes == null) {
            return new HashSet<String>(0);
        }
        return this.authorizedGrantTypes;
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        if (this.webServerRedirectUri == null
                || this.webServerRedirectUri.isEmpty()) {
            return new HashSet<String>(0);
        }
        return new HashSet<String>(Arrays.asList(this.webServerRedirectUri));
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        if (this.authorities == null || this.authorities.isEmpty()) {
            return new ArrayList<GrantedAuthority>(0);
        }
        return this.authorities.stream().map(
                authority -> new wse.account.oauth2.client.service.GrantedAuthority(
                        authority))
                .collect(Collectors.toList());
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return null;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return null;
    }

    @Override
    public boolean isAutoApprove(String scope) {
        return true;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return new HashMap<String, Object>(0);
    }

    public static final class Parser {

        public static ClientDetails fromJpa(OauthClientDetail client) {
            if (client == null) {
                return null;
            }
            return new ClientDetails(client.getId(),
                    client.getResourceIds(), client.getClientSecret(),
                    client.getScope(), client.getAuthorizedGrantTypes(),
                    client.getWebServerRedirectUri(), client.getAuthorities());
        }

    }

}
