package wse.account.oauth2.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

import wse.account.oauth2.client.data.ClientRepository;
import wse.account.oauth2.client.data.OauthClientDetail;

@Service
public class ClientDetailsService
        implements
            org.springframework.security.oauth2.provider.ClientDetailsService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public ClientDetails loadClientByClientId(String clientId)
            throws ClientRegistrationException {
        OauthClientDetail client = clientRepository.findOneById(clientId);
        return ClientDetails.Parser.fromJpa(client);
    }

}
