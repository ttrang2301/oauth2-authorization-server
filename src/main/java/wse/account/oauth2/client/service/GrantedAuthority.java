package wse.account.oauth2.client.service;

public class GrantedAuthority
        implements
            org.springframework.security.core.GrantedAuthority {

    private static final long serialVersionUID = 7484255795187998604L;

    public String clientRole;

    public GrantedAuthority(String clientRole) {
        super();
        this.clientRole = clientRole;
    }

    @Override
    public String getAuthority() {
        return this.clientRole;
    }

}
