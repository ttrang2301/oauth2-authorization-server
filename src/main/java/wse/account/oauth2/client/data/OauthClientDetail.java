package wse.account.oauth2.client.data;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import wse.account.data.converter.StringToSetStringConverter;

/**
 * The persistent class for the oauth_client_details database table.
 *
 */
@Entity
@Table(name = "oauth_client_details")
@NamedQuery(name = "OauthClientDetail.findAll", query = "SELECT o FROM OauthClientDetail o")
public class OauthClientDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "client_id")
    private String id;

    @Column(name = "access_token_validity")
    private int accessTokenValidity;

    @Column(name = "additional_information")
    private String additionalInformation;

    @Convert(converter = StringToSetStringConverter.class)
    private Set<String> authorities;

    @Column(name = "authorized_grant_types")
    @Convert(converter = StringToSetStringConverter.class)
    private Set<String> authorizedGrantTypes;

    private Byte autoapprove;

    @Column(name = "client_secret")
    private String clientSecret;

    @Column(name = "refresh_token_validity")
    private Integer refreshTokenValidity;

    @Column(name = "resource_ids")
    @Convert(converter = StringToSetStringConverter.class)
    private Set<String> resourceIds;

    private String scope;

    @Column(name = "web_server_redirect_uri")
    private String webServerRedirectUri;

    public OauthClientDetail() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAccessTokenValidity() {
        return this.accessTokenValidity;
    }

    public void setAccessTokenValidity(int accessTokenValidity) {
        this.accessTokenValidity = accessTokenValidity;
    }

    public String getAdditionalInformation() {
        return this.additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public Set<String> getAuthorities() {
        return this.authorities;
    }

    public void setAuthorities(Set<String> authorities) {
        this.authorities = authorities;
    }

    public Set<String> getAuthorizedGrantTypes() {
        return this.authorizedGrantTypes;
    }

    public void setAuthorizedGrantTypes(Set<String> authorizedGrantTypes) {
        this.authorizedGrantTypes = authorizedGrantTypes;
    }

    public Byte getAutoapprove() {
        return this.autoapprove;
    }

    public void setAutoapprove(Byte autoapprove) {
        this.autoapprove = autoapprove;
    }

    public String getClientSecret() {
        return this.clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public Integer getRefreshTokenValidity() {
        return this.refreshTokenValidity;
    }

    public void setRefreshTokenValidity(Integer refreshTokenValidity) {
        this.refreshTokenValidity = refreshTokenValidity;
    }

    public Set<String> getResourceIds() {
        return this.resourceIds;
    }

    public void setResourceIds(Set<String> resourceIds) {
        this.resourceIds = resourceIds;
    }

    public String getScope() {
        return this.scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getWebServerRedirectUri() {
        return this.webServerRedirectUri;
    }

    public void setWebServerRedirectUri(String webServerRedirectUri) {
        this.webServerRedirectUri = webServerRedirectUri;
    }

}