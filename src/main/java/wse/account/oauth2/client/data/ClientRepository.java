package wse.account.oauth2.client.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<OauthClientDetail, String> {
    public OauthClientDetail findOneById(String id);
}
