package wse.account.oauth2;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfigurer
        extends
            org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    // ======================= Configure Authentication

    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints)
            throws Exception {
        // @formatter:off
        endpoints
            .authenticationManager(authenticationManager)
            .tokenStore(tokenStore());
        // @formatter:on
    }

    @Bean
    public TokenStore tokenStore() {
        return new JdbcTokenStore(dataSource);
    }

    // ======================= Configure service for Token

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer)
            throws Exception {
        oauthServer
        // @formatter:off
            // TODO I don't know what does this mean???
            .tokenKeyAccess("permitAll()")

            // We here defines the security constraints on the token endpoint.
            // We set it up to isAuthenticated, which returns true if the user is not anonymous
            .checkTokenAccess("isAuthenticated()");
        // @formatter:on
    }

    // ======================= Configure Client Details Loader

//    @Autowired
//    private ClientDetailsService clientDetailsService;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients)
            throws Exception {
//        clients.withClientDetails(clientDetailsService());
        clients.jdbc(dataSource);
    }

//    @Bean
//    public ClientDetailsService clientDetailsService() {
//        return new JdbcClientDetailsService(dataSource);
//    }

}
