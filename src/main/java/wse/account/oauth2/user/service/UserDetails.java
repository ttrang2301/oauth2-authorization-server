package wse.account.oauth2.user.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;

import wse.account.data.UserRole;
import wse.account.oauth2.user.data.User;

public class UserDetails
        implements
            org.springframework.security.core.userdetails.UserDetails {

    private static final long serialVersionUID = -7486977684599588291L;

    private List<UserRole> roles;
    private String password;
    private String username;
    private Boolean deactive;

    public UserDetails(List<UserRole> roles, String password, String username,
            Boolean deactive) {
        super();
        this.roles = roles;
        this.password = password;
        this.username = username;
        this.deactive = deactive;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        if (this.roles == null || this.roles.isEmpty()) {
            return new ArrayList<GrantedAuthority>(0);
        }
        return this.roles.stream().map(
                role -> new wse.account.oauth2.user.service.GrantedAuthority(
                        role))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return (this.deactive != null && !this.deactive);
    }

    @Override
    public boolean isAccountNonLocked() {
        return (this.deactive != null && !this.deactive);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return (this.deactive != null && !this.deactive);
    }

    @Override
    public boolean isEnabled() {
        return (this.deactive != null && !this.deactive);
    }

    public static final class Parser {

        public static UserDetails fromJpa(User user) {
            if (user == null) {
                return null;
            }
            return new UserDetails(user.getUserRoles(), user.getPassword(),
                    user.getUsername(), user.getDeactive());
        }

    }

}
