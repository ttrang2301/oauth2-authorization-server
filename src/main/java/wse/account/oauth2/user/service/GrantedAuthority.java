package wse.account.oauth2.user.service;

import wse.account.data.UserRole;

public class GrantedAuthority
        implements
            org.springframework.security.core.GrantedAuthority {

    private static final long serialVersionUID = -8389652101768918330L;

    private UserRole role;

    public GrantedAuthority(UserRole role) {
        super();
        this.role = role;
    }

    @Override
    public String getAuthority() {
        if (this.role == null) {
            return "";
        }
        return this.role.getDept().getDeptId() + "_"
                + this.role.getRole().getRoleId();
    }

}
