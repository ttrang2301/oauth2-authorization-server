package wse.account.oauth2.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import wse.account.oauth2.user.data.User;
import wse.account.oauth2.user.data.UserRepository;

@Service
public class UserDetailsService
        implements
            org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    /**
     * Define the PasswordEncoder. Ref:
     * {@link http://www.baeldung.com/spring-security-registration-password-encoding-bcrypt}
     *
     * @return
     */
    @Bean(name = "passwordEncoder")
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        User user = userRepository.findOneByUsername(username);
        return UserDetails.Parser.fromJpa(user);
    }

}
